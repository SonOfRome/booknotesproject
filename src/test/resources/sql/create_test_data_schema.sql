DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user;

create table author
(
    AUTHOR_ID  int auto_increment,
    LAST_NAME  VARCHAR(45) not null,
    FIRST_NAME VARCHAR(45) not null,
    NICK_NAME  VARCHAR(45),
    BIRTH_DATE DATE,
    FULL_NAME  VARCHAR(45) not null,
    constraint AUTHOR_PK
        primary key (AUTHOR_ID)
);

create table role
(
    ROLE_ID          int auto_increment,
    ROLE_NAME        VARCHAR(45) not null,
    ROLE_DESCRIPTION VARCHAR(45) not null,
    constraint role_pk
        primary key (ROLE_ID)
);
create table user
(
    USER_ID    int auto_increment,
    LAST_NAME  VARCHAR(45) not null,
    FIRST_NAME VARCHAR(45) not null,
    LOGIN      VARCHAR(45) not null,
    PASSWORD   VARCHAR(45) not null,
    ROLE_ID    int         not null,
    constraint USER_PK
        primary key (USER_ID),
    constraint USER_ROLE_ROLE_ID_FK
        foreign key (ROLE_ID) references role (ROLE_ID)
);

insert into role
values (1, 'testRole', 'test description');

insert into user
values (1, 'testUserSurname', 'testUserFirstName', 'login', '123456', 1);

insert into author
values (1, 'testAuthorSurname', 'testAuthorFirstName', 'testAuthorNickName', '1997-02-05', 'testFullName');

