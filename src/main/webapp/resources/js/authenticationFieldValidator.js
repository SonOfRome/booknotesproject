'use strict';

function doVerification(loginInputField, passwordInputField) {
    if (!verifyInputRangeFor(loginInputField)) {
        loginInputField.style.borderColor = 'red';
    }
    if (!verifyInputRangeFor(passwordInputField)) {
        passwordInputField.style.borderColor = 'red';
    }
}

function verifyInputRangeFor(inputField) {
    return inputField.value.length > 0 && inputField.value.length <= 45;
}