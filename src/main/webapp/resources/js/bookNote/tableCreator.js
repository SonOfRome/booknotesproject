'use strict';

function getLabelNames(originalPersonageTable) {
    let labelElements = originalPersonageTable.getElementsByTagName('label');
    let labels = [];

    Array.from(labelElements)
        .map(labelElement => labelElement.innerHTML)
        .forEach(label => labels.push(label));
    return labels;
}

function getAttributeNames(originalPersonageTable) {
    let inputElements = originalPersonageTable.getElementsByTagName('input');
    let attributeNames = [];

    Array.from(inputElements)
        .map(inputElement => inputElement.getAttribute('name'))
        .forEach(attributeName => attributeNames.push(attributeName));
    return attributeNames;
}

function createNewTable(labels, attributes) {
    let table = document.createElement('table');
    let cellIndex = 0;
    for (let rowIndex = 0; rowIndex < labels.length; rowIndex++) {
        let labelRow = table.insertRow(rowIndex);
        let labelCell = labelRow.insertCell(cellIndex);
        let inputCell = labelRow.insertCell(++cellIndex);

        labelCell.innerHTML = '<label>' + labels[rowIndex] + '</label>';
        inputCell.innerHTML = '<input type="text" name="' + attributes[rowIndex] + '"/>';
        cellIndex = 0;
    }
    return table;
}