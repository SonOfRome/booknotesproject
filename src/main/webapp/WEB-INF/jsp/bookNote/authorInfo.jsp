<table>
    <tr>
        <td><label>Last name</label></td>
        <td><input type="text" name="authorLastName"/></td>
    </tr>
    <tr>
        <td><label>First name</label></td>
        <td><input type="text" name="authorFirstName"/></td>
    </tr>
    <tr>
        <td><label>Nickname</label></td>
        <td><input type="text" name="authorNickName"/></td>
    </tr>
    <tr>
        <td><label>Date of birth</label></td>
        <td><input type="date" name="authorBirthDate"/></td>
    </tr>
    <tr>
        <td><label>Full name</label></td>
        <td><input type="text" name="authorFullName"/></td>
    </tr>
</table>