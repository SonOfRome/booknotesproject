<table id="originalTable">
    <tr>
        <td><label>Full name</label></td>
        <td><input type="text" name="personageFullName"/></td>
    </tr>
    <tr id="role">
        <td><label>Role</label></td>
        <td><input type="text" name="personageRole"/></td>
    </tr>
</table>
<button id="addPersonageButton"><label>Add personage</label></button>
<button id="addQuotation"><label>Add quotation</label></button>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        document.getElementById('addPersonageButton').addEventListener('click', event => {
            event.preventDefault();
            let originalPersonageTable = document.getElementById('originalTable');
            let labels = getLabelNames(originalPersonageTable);
            let attributes = getAttributeNames(originalPersonageTable);
            let newTable = createNewTable(labels, attributes);
            document.getElementById('bookNoteForm').appendChild(newTable);
            document.getElementById('personageFieldSet')
                .insertBefore(newTable, document.getElementById('addPersonageButton'));
        });
        document.getElementById('addQuotation').addEventListener('click', event => {
           event.preventDefault();
           let quotationRow = document.createElement('tr');
           let quotationLabelCell = quotationRow.insertCell(0);
           let quotationInputCell = quotationRow.insertCell(1);
           quotationLabelCell.innerHTML = '<label>Quotation </label>';
           quotationInputCell.innerHTML = '<input type="text" name="quotation"/>';
           document.getElementById('addQuotation').parentNode.insertBefore(quotationRow,
               document.getElementById('addPersonageButton'));
        });
    });
</script>
