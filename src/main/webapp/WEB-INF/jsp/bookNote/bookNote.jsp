<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/js/bookNote/tableCreator.js"/>"></script>
<form id="bookNoteForm" action="addBookNote" method="POST">
    <fieldset>
        <legend>Author information</legend>
        <tiles:insertAttribute name="authorInfo"/>
    </fieldset>
    <fieldset>
        <legend>Book information</legend>
        <tiles:insertAttribute name="bookInfo"/>
    </fieldset>
    <fieldset>
        <legend>Genre information</legend>
        <tiles:insertAttribute name="genreInfo"/>
    </fieldset>
    <fieldset id="personageFieldSet">
        <legend>Personage information</legend>
        <tiles:insertAttribute name="personageInfo"/>
    </fieldset>
    <input type="submit" value="Add a new book note"/>
</form>