<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
    <title>Personal cabinet</title>
</head>
<body>
<c:choose>
    <c:when test="${role eq 'ADMIN'}">
        <tiles:insertAttribute name="adminCabinet"/>
    </c:when>
    <c:otherwise>
        <tiles:insertAttribute name="userCabinet"/>
    </c:otherwise>
</c:choose>
</body>
</html>
