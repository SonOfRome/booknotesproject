<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Book Notes - Welcome Page</title>
</head>
<body>
<div id="mainBlock">
    <ul>
        <li>
            <p id="createBookNote">
                <a href="${pageContext.request.contextPath}/createBookNote">Create a new book note</a>
            </p>
        </li>
        <li>View my book notes</li>
    </ul>
</div>
</body>
</html>
