<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false"%>
<html>
<head>
    <title>Book Notes Application - Welcome Page</title>
</head>
<script src="${pageContext.request.contextPath}/resources/js/authenticationFieldValidator.js"></script>
<body>
<h2>Hi! Welcome in Book Notes. Please sign in to continue.</h2>
<form action="doUserVerification" method="POST">
    <table>
        <tr>
            <td><label id="loginLabel">Login</label></td>
            <td><input type="text" name="loginInput" id="loginInputField"/></td>
        </tr>
        <tr>
            <td><label id="passwordLabel">Password</label></td>
            <td><input type="password" name="passwordInput" id="passwordInputField"/></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Sign in" id="submitButton"/></td>
        </tr>
    </table>
</form>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        let loginInputField = document.getElementById('loginInputField');
        let passwordInputField = document.getElementById('passwordInputField');
        document.getElementById('submitButton').addEventListener('click', event => {
            if (loginInputField.value.length === 0 || passwordInputField.value.length === 0) {
                event.preventDefault();
                doVerification(loginInputField, passwordInputField);
            }
        });
        loginInputField.addEventListener('focusin', () => {
           loginInputField.style.borderColor = '';
        });
        passwordInputField.addEventListener('focusin', () => {
            passwordInputField.style.borderColor = '';
        });
    });
</script>
</body>
</html>
