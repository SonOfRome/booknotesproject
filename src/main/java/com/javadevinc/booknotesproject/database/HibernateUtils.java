package com.javadevinc.booknotesproject.database;

import com.javadevinc.booknotesproject.model.Author;
import com.javadevinc.booknotesproject.model.Book;
import com.javadevinc.booknotesproject.model.Genre;
import com.javadevinc.booknotesproject.model.Personage;
import com.javadevinc.booknotesproject.model.PersonalUserInfo;
import com.javadevinc.booknotesproject.model.Quote;
import com.javadevinc.booknotesproject.model.Role;
import com.javadevinc.booknotesproject.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
        } catch (Exception e) {
            System.err.println("Initial SessionFactory creation failed:" + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
