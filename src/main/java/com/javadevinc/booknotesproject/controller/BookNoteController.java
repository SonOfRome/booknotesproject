package com.javadevinc.booknotesproject.controller;

import com.javadevinc.booknotesproject.service.BookNoteService;
import com.javadevinc.booknotesproject.service.activemq.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BookNoteController {
    private BookNoteService bookNoteService;
    private MessageService messageService;

    @Autowired
    public BookNoteController(BookNoteService bookNoteService, MessageService messageService) {
        this.bookNoteService = bookNoteService;
        this.messageService = messageService;
    }

    @RequestMapping(value = "/createBookNote", method = RequestMethod.GET)
    public String createBookNote() {
        return "bookNote";
    }

    @RequestMapping(value = "/addBookNote", method = RequestMethod.POST)
    public String addBookNote(@RequestBody String bookNoteInfo) {
        bookNoteService.addBookNote(getBookNoteData(bookNoteInfo));
        messageService.sendBookNoteNotification("Book note was added");
        return "personalCabinet";
    }

    private Map<String, String> getBookNoteData(String bookNoteInfo) {
        Map<String, String> bookNoteData = new HashMap<>();
        String[] params = bookNoteInfo.split("&");
        for(String param : params) {
            String[] keyValueArray = param.split("=");
            if (keyValueArray.length == 2) {
                bookNoteData.put(keyValueArray[0], keyValueArray[1]);
            } else {
                bookNoteData.put(keyValueArray[0], null);
            }
        }
        return bookNoteData;
    }
}
