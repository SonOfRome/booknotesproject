package com.javadevinc.booknotesproject.controller;

import com.javadevinc.booknotesproject.model.Role;
import com.javadevinc.booknotesproject.model.User;
import com.javadevinc.booknotesproject.service.security.AuthenticationServiceImpl;
import com.javadevinc.booknotesproject.service.security.AuthorizationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class UserCredentialsAndRightsValidationController {
    private AuthenticationServiceImpl authenticationService;
    private AuthorizationServiceImpl authorizationService;

    @Autowired
    public UserCredentialsAndRightsValidationController(AuthenticationServiceImpl authenticationService,
                                                        AuthorizationServiceImpl authorizationService) {
        this.authenticationService = authenticationService;
        this.authorizationService = authorizationService;
    }

    @RequestMapping(value = "/doUserVerification", method = RequestMethod.POST)
    public String doUserVerification(@RequestParam("loginInput") String login,
                                     @RequestParam("passwordInput") String password,
                                     HttpSession httpSession) {
        Optional<User> user = authenticationService.getUserWith(login, password);
        if (user.isPresent()) {
            Optional<Role> role = authorizationService.getUserRole(user.get());
            role.ifPresent(userRole -> {
                httpSession.setAttribute("isLoggedIn", true);
                httpSession.setAttribute("role", userRole.getName());
            });
        }
        return user.isPresent() ? "personalCabinet" : "error/noUserExist";
    }
}
