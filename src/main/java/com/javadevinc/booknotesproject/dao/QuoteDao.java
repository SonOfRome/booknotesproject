package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Quote;

public interface QuoteDao extends Dao<Quote> {
}
