package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.PersonalUserInfo;

public interface PersonalUserInfoDao extends Dao<PersonalUserInfo> {
}
