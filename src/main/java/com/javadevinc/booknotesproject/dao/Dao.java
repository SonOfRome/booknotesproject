package com.javadevinc.booknotesproject.dao;

import java.util.Optional;

public interface Dao<T> {
    void addEntity(T entity);

    void updateEntity(T entity);

    void deleteEntity(T entity);

    Optional<T> getEntityById(Integer id);
}
