package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Role;
import com.javadevinc.booknotesproject.model.User;

import java.util.Optional;

public interface RoleDao extends Dao<Role> {
    Optional<Role> getRoleFor(User user);
}
