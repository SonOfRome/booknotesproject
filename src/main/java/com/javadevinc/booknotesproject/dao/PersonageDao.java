package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Personage;

public interface PersonageDao extends Dao<Personage> {
}
