package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Book;

public interface BookDao extends Dao<Book> {
}
