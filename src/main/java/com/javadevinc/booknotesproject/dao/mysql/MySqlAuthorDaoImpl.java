package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.AuthorDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.Author;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class MySqlAuthorDaoImpl implements AuthorDao {
    @Override
    public void addEntity(Author entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to add a new author with id %d", entity.getAuthorId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(Author entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to update the existing author with id %d",
                    entity.getAuthorId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(Author entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to delete the existing author with id %d",
                    entity.getAuthorId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Author> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<Author> author = Optional.empty();
        try {
            author = Optional.ofNullable(session.get(Author.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to get the author with id %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return author;
    }
}
