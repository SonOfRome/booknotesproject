package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.UserDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MySqlUserDaoImpl implements UserDao {
    @Override
    public void addEntity(User entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to add a new user with id %d", entity.getUserId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(User entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to update the existing user with id %d",
                    entity.getUserId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(User entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An error occurs when trying to delete the existing user with id %d",
                    entity.getUserId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<User> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<User> user = Optional.empty();
        try {
            user = Optional.ofNullable(session.get(User.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to get the user with id %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return user;
    }


    @Override
    public Optional<User> getUserByHisCredentials(String login, String password) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<User> user = Optional.empty();
        try {
            Criteria criteria = session.createCriteria(User.class);
            Criterion loginCriterion = Restrictions.eq("login", login);
            Criterion passwordCriterion = Restrictions.eq("password", password);

            LogicalExpression andExp = Restrictions.and(loginCriterion, passwordCriterion);
            criteria.add(andExp);

            List<User> userList = criteria.list();
            if (!userList.isEmpty()) {
                user = Optional.ofNullable(userList.get(0));
            }
            transaction.commit();
        } catch (HibernateException exception) {
            System.out.printf("An error occurred when getting an entity with login=%s", login);
            exception.getStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
        return user;
    }
}
