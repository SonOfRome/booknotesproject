package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.RoleDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.Role;
import com.javadevinc.booknotesproject.model.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Optional;

public class MySqlRoleDaoImpl implements RoleDao {
    @Override
    public void addEntity(Role entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to add a new role with id = %d", entity.getRoleId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(Role entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to update the existing role with id = %d",
                    entity.getRoleId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(Role entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to delete the existing role with id = %d",
                    entity.getRoleId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Role> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Optional<Role> role = Optional.empty();
        Transaction transaction = session.beginTransaction();
        try {
            role = Optional.ofNullable(session.get(Role.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while getting the existing role with id = %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return role;
    }

    @Override
    public Optional<Role> getRoleFor(User user) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<Role> role = Optional.empty();
        try {
            Criteria criteria = session.createCriteria(Role.class);
            criteria.add(Restrictions.eq("roleId", user.getRole().getRoleId()));
            List<Role> roleList = criteria.list();
            if (!roleList.isEmpty()) {
                role = Optional.ofNullable(roleList.get(0));
            }
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception while getting entity by id = %d", user.getUserId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return role;
    }
}
