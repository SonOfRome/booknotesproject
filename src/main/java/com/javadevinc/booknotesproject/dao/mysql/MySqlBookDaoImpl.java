package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.BookDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.Book;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class MySqlBookDaoImpl implements BookDao {
    @Override
    public void addEntity(Book entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while adding a new book with id = %d", entity.getBookId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(Book entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while updating the existing book with id = %d",
                    entity.getBookId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(Book entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while deleting the existing book with id = %d",
                    entity.getBookId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Book> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<Book> book = Optional.empty();
        try {
            book = Optional.ofNullable(session.get(Book.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while getting the existing book with id %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return book;
    }
}
