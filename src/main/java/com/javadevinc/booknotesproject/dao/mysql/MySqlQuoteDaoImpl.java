package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.QuoteDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.Quote;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class MySqlQuoteDaoImpl implements QuoteDao {
    @Override
    public void addEntity(Quote entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while adding a new quote with id = %d",
                    entity.getQuoteId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(Quote entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while updating the existing quote with id = %d",
                    entity.getQuoteId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(Quote entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while deleting the existing quote with id = %d",
                    entity.getQuoteId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Quote> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<Quote> user = Optional.empty();
        try {
            user = Optional.ofNullable(session.get(Quote.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to get the quote with id = %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
}
