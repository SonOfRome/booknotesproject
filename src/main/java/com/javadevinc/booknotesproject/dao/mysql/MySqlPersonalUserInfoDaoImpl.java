package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.PersonalUserInfoDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.PersonalUserInfo;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class MySqlPersonalUserInfoDaoImpl implements PersonalUserInfoDao {
    @Override
    public void addEntity(PersonalUserInfo entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while adding a new personal info with id = %d",
                    entity.getPersonalUserInfoId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(PersonalUserInfo entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while updating the existing personal info with id = %d",
                    entity.getPersonalUserInfoId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(PersonalUserInfo entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while deleting the existing personal info with id = %d",
                    entity.getPersonalUserInfoId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<PersonalUserInfo> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<PersonalUserInfo> personage = Optional.empty();
        try {
            personage = Optional.ofNullable(session.get(PersonalUserInfo.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs while getting the existing personal info with id = %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return personage;
    }
}
