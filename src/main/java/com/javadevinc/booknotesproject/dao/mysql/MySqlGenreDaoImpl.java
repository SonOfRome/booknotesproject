package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.GenreDao;
import com.javadevinc.booknotesproject.database.HibernateUtils;
import com.javadevinc.booknotesproject.model.Genre;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class MySqlGenreDaoImpl implements GenreDao {
    @Override
    public void addEntity(Genre entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to add a new genre with id %d", entity.getGenreId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateEntity(Genre entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to add the existing genre with id %d",
                    entity.getGenreId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteEntity(Genre entity) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to delete the existing genre with id %d",
                    entity.getGenreId());
            exception.getStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Genre> getEntityById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Optional<Genre> genre = Optional.empty();
        try {
            genre = Optional.ofNullable(session.get(Genre.class, id));
            transaction.commit();
        } catch (HibernateException exception) {
            transaction.rollback();
            System.out.printf("An exception occurs when trying to get th existing genre with id = %d", id);
            exception.getStackTrace();
        } finally {
            session.close();
        }
        return genre;
    }
}
