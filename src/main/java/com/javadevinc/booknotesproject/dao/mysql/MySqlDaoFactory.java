package com.javadevinc.booknotesproject.dao.mysql;

import com.javadevinc.booknotesproject.dao.AuthorDao;
import com.javadevinc.booknotesproject.dao.BookDao;
import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.dao.GenreDao;
import com.javadevinc.booknotesproject.dao.PersonageDao;
import com.javadevinc.booknotesproject.dao.PersonalUserInfoDao;
import com.javadevinc.booknotesproject.dao.QuoteDao;
import com.javadevinc.booknotesproject.dao.RoleDao;
import com.javadevinc.booknotesproject.dao.UserDao;
import org.springframework.stereotype.Repository;

@Repository
public class MySqlDaoFactory implements DaoFactory {
    @Override
    public AuthorDao getAuthorDao() {
        return new MySqlAuthorDaoImpl();
    }

    @Override
    public BookDao getBookDao() {
        return new MySqlBookDaoImpl();
    }

    @Override
    public GenreDao getGenreDao() {
        return new MySqlGenreDaoImpl();
    }

    @Override
    public PersonageDao getPersonageDao() {
        return new MySqlPersonageDaoImpl();
    }

    @Override
    public PersonalUserInfoDao getPersonalUserInfoDao() {
        return new MySqlPersonalUserInfoDaoImpl();
    }

    @Override
    public QuoteDao getQuoteDao() {
        return new MySqlQuoteDaoImpl();
    }

    @Override
    public RoleDao getRoleDao() {
        return new MySqlRoleDaoImpl();
    }

    @Override
    public UserDao getUserDao() {
        return new MySqlUserDaoImpl();
    }
}
