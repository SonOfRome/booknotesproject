package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.User;

import java.util.Optional;

public interface UserDao extends Dao<User> {
    Optional<User> getUserByHisCredentials(String login, String password);
}
