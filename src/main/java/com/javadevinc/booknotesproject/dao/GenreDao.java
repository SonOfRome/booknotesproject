package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Genre;

public interface GenreDao extends Dao<Genre> {

}
