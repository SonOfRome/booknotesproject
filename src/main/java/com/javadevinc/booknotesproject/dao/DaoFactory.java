package com.javadevinc.booknotesproject.dao;

public interface DaoFactory {
    AuthorDao getAuthorDao();

    BookDao getBookDao();

    GenreDao getGenreDao();

    PersonageDao getPersonageDao();

    PersonalUserInfoDao getPersonalUserInfoDao();

    QuoteDao getQuoteDao();

    RoleDao getRoleDao();

    UserDao getUserDao();
}
