package com.javadevinc.booknotesproject.dao;

import com.javadevinc.booknotesproject.model.Author;

public interface AuthorDao extends Dao<Author> {

}
