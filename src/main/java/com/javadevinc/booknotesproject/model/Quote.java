package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "quote")
public class Quote implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "QUOTE_ID", unique = true, nullable = false)
    private Integer quoteId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BOOK_ID")
    private Book book;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSONAGE_ID")
    private Personage personage;
    @Column(name = "QUOTATION", nullable = false, length = 1000)
    private String quotation;

    public Quote() {

    }

    public Integer getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Integer quoteId) {
        this.quoteId = quoteId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Personage getPersonage() {
        return personage;
    }

    public void setPersonage(Personage personage) {
        this.personage = personage;
    }

    public String getQuotation() {
        return quotation;
    }

    public void setQuotation(String quotation) {
        this.quotation = quotation;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Quote quote = (Quote) object;
        return Objects.equals(quoteId, quote.getQuoteId())
                && Objects.equals(book.getBookId(), quote.getBook().getBookId())
                && Objects.equals(personage.getPersonageId(), quote.getPersonage().getPersonageId())
                && Objects.equals(quotation, quote.getQuotation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(quoteId, book.getBookId(), personage.getPersonageId(), quotation);
    }

    @Override
    public String toString() {
        return String.format("Quote: id = %d, bookId = %d, personageId = %d, quotation = %s",
                quoteId, book.getBookId(), personage.getPersonageId(), quotation);
    }
}
