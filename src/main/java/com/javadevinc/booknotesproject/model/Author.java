package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "author")
public class Author implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AUTHOR_ID", unique = true, nullable = false)
    private Integer authorId;
    @Column(name = "LAST_NAME", nullable = false, length = 45)
    private String lastName;
    @Column(name = "FIRST_NAME", nullable = false, length = 45)
    private String firstName;
    @Column(name = "NICK_NAME", length = 45)
    private String nickName;
    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;
    @Column(name = "FULL_NAME", nullable = false, length = 45)
    private String fullName;

    public Author() {

    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Author author = (Author) object;
        return Objects.equals(authorId, author.getAuthorId())
                && Objects.equals(lastName, author.getLastName())
                && Objects.equals(firstName, author.getFirstName())
                && Objects.equals(nickName, author.getNickName())
                && Objects.equals(birthDate, author.getBirthDate())
                && Objects.equals(fullName, author.getFullName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, lastName, firstName, nickName, birthDate, fullName);
    }

    @Override
    public String toString() {
        return String.format("Author: id = %d, fullName = %s", authorId, fullName);
    }
}
