package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "role")
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID", unique = true, nullable = false)
    private Integer roleId;
    @Column(name = "NAME", nullable = false, length = 45)
    private String name;
    @Column(name = "DESCRIPTION", nullable = false, length = 45)
    private String description;

    public Role() {

    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Role role = (Role) object;
        return Objects.equals(roleId, role.getRoleId())
                && Objects.equals(name, role.getName())
                && Objects.equals(description, role.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, name, description);
    }

    @Override
    public String toString() {
        return String.format("Role: id = %d, name = %s, description = %s", roleId, name, description);
    }
}
