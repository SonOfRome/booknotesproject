package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "personage")
public class Personage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERSONAGE_ID", unique = true, nullable = false)
    private Integer personageId;
    @Column(name = "FULL_NAME", nullable = false, length = 60)
    private String fullName;
    @Column(name = "ROLE", nullable = false, length = 500)
    private String role;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BOOK_ID")
    private Book book;

    public Personage() {

    }

    public Integer getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Integer personageId) {
        this.personageId = personageId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Personage personage = (Personage) object;
        return Objects.equals(personageId, personage.getPersonageId())
                && Objects.equals(fullName, personage.getFullName())
                && Objects.equals(role, personage.getRole())
                && Objects.equals(book.getBookId(), personage.getBook().getBookId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(personageId, fullName, role, book.getBookId());
    }

    @Override
    public String toString() {
        return String.format("Personage: id = %d, fullName = %s, bookId = %d",
                personageId, fullName, book.getBookId());
    }
}
