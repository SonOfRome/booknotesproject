package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "genre")
public class Genre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GENRE_ID", unique = true, nullable = false)
    private Integer genreId;
    @Column(name = "NAME", nullable = false, length = 45)
    private String name;
    @Column(name = "DESCRIPTION", nullable = false, length = 1000)
    private String description;

    public Genre() {

    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Genre genre = (Genre) object;
        return Objects.equals(genreId, genre.getGenreId())
                && Objects.equals(name, genre.getName())
                && Objects.equals(description, genre.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(genreId, name, description);
    }

    @Override
    public String toString() {
        return String.format("Genre: id = %d, name = %s, desc: %s", genreId, name, description);
    }
}
