package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Integer userId;
    @Column(name = "LAST_NAME", nullable = false, length = 45)
    private String lastName;
    @Column(name = "FIRST_NAME", nullable = false, length = 45)
    private String firstName;
    @Column(name = "LOGIN", unique = true, nullable = false, length = 45)
    private String login;
    @Column(name = "PASSWORD", unique = true, nullable = false, length = 45)
    private String password;
    @OneToOne
    @JoinColumn(name = "ROLE_ID")
    private Role role;

    public User() {

    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        User user = (User) object;
        return Objects.equals(userId, user.getUserId())
                && Objects.equals(lastName, user.getLastName())
                && Objects.equals(firstName, user.getFirstName())
                && Objects.equals(login, user.getLogin())
                && Objects.equals(password, user.getPassword())
                && Objects.equals(role.getRoleId(), user.getRole().getRoleId());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + userId.hashCode();
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + role.getRoleId().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("User: id = %d, login = %s", userId, login);
    }
}
