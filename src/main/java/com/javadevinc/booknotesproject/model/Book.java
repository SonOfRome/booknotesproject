package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "book")
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BOOK_ID", unique = true, nullable = false)
    private Integer bookId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTHOR_ID")
    private Author author;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GENRE_ID")
    private Genre genre;
    @Column(name = "PUBLISH_YEAR", nullable = false)
    private Integer publishYear;
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    public Book() {

    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Integer getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Book book = (Book) object;
        return Objects.equals(bookId, book.getBookId())
                && Objects.equals(author.getAuthorId(), book.getAuthor().getAuthorId())
                && Objects.equals(genre.getGenreId(), book.getGenre().getGenreId())
                && Objects.equals(publishYear, book.getPublishYear())
                && Objects.equals(name, book.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId, author.getAuthorId(), genre.getGenreId(), publishYear, name);
    }

    @Override
    public String toString() {
        return String.format("Book: id = %d, author = %d, name = %s", bookId, author.getAuthorId(), name);
    }
}
