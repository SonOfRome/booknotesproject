package com.javadevinc.booknotesproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "personal_user_info")
public class PersonalUserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERSONAL_USER_INFO_ID", unique = true, nullable = false)
    private Integer personalUserInfoId;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;
    @Column(name = "BIRTH_DATE", nullable = false)
    private LocalDate birthDate;
    @Column(name = "CITY", nullable = false, length = 45)
    private String city;
    @Column(name = "EMAIL", unique = true, nullable = false, length = 45)
    private String email;

    public PersonalUserInfo() {

    }

    public Integer getPersonalUserInfoId() {
        return personalUserInfoId;
    }

    public void setPersonalUserInfoId(Integer personalUserInfoId) {
        this.personalUserInfoId = personalUserInfoId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        PersonalUserInfo personalUserInfo = (PersonalUserInfo) object;
        return Objects.equals(personalUserInfoId, personalUserInfo.getPersonalUserInfoId())
                && Objects.equals(user.getUserId(), personalUserInfo.getUser().getUserId())
                && Objects.equals(birthDate, personalUserInfo.getBirthDate())
                && Objects.equals(city, personalUserInfo.getCity())
                && Objects.equals(email, personalUserInfo.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalUserInfoId, user.getUserId(), birthDate, city, email);
    }

    @Override
    public String toString() {
        return String.format("PersonalUserInfo: infoId = %d, userId = %d," +
                        " birthDate = %tY%tB%tA, city = %s, email = %s", personalUserInfoId, user.getUserId(), birthDate,
                city, email);
    }
}
