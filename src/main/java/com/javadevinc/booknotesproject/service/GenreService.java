package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.model.Genre;

public interface GenreService {
    void addGenre(Genre genre);
}
