package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImpl implements AuthorService {
    private DaoFactory daoFactory;

    @Autowired
    public AuthorServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }


    @Override
    public void addAuthor(Author author) {
        daoFactory.getAuthorDao().addEntity(author);
    }
}
