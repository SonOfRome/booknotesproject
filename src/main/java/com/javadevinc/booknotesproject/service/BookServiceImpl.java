package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {
    private DaoFactory daoFactory;

    @Autowired
    public BookServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void addBook(Book book) {
        daoFactory.getBookDao().addEntity(book);
    }
}
