package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.model.Author;


public interface AuthorService {
    void addAuthor(Author author);
}
