package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.model.Book;

public interface BookService {
    void addBook(Book book);
}
