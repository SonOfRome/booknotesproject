package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.model.Author;
import com.javadevinc.booknotesproject.model.Book;
import com.javadevinc.booknotesproject.model.Genre;
import com.javadevinc.booknotesproject.model.Personage;
import com.javadevinc.booknotesproject.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;

@Service
public class BookNoteServiceImpl implements BookNoteService {
    private AuthorService authorService;
    private GenreService genreService;
    private BookService bookService;
    private PersonageService personageService;
    private QuoteService quoteService;

    @Autowired
    public BookNoteServiceImpl(AuthorService authorService, GenreService genreService, BookService bookService,
                               PersonageService personageService, QuoteService quoteService) {
        this.authorService = authorService;
        this.genreService = genreService;
        this.bookService = bookService;
        this.personageService = personageService;
        this.quoteService = quoteService;
    }

    @Override
    public void addBookNote(Map<String, String> bookNoteData) {
        Author author = fillAuthorObjectWith(bookNoteData);
        Genre genre = fillGenreObjectWith(bookNoteData);
        Book book = fillBookObjectWith(bookNoteData, author, genre);
        Personage personage = fillPersonageObjectWith(bookNoteData, book);
        Quote quote = fillQuoteObjectWith(bookNoteData, book, personage);

        authorService.addAuthor(author);
        genreService.addGenre(genre);
        bookService.addBook(book);
        personageService.addPersonage(personage);
        quoteService.addQuote(quote);
    }

    private Author fillAuthorObjectWith(Map<String, String> bookNoteData) {
        Author author = new Author();

        author.setLastName(bookNoteData.get("authorLastName"));
        author.setFirstName(bookNoteData.get("authorFirstName"));
        author.setNickName(bookNoteData.get("authorNickName"));
        author.setBirthDate(LocalDate.parse(bookNoteData.get("authorBirthDate")));
        author.setFullName(bookNoteData.get("authorFullName"));

        return author;
    }

    private Genre fillGenreObjectWith(Map<String, String> bookNoteData) {
        Genre genre = new Genre();

        genre.setName(bookNoteData.get("genreName"));
        genre.setDescription(bookNoteData.get("genreDescription"));

        return genre;
    }

    private Book fillBookObjectWith(Map<String, String> bookNoteData, Author author, Genre genre) {
        Book book = new Book();

        book.setAuthor(author);
        book.setGenre(genre);
        book.setName(bookNoteData.get("bookName"));
        book.setPublishYear(Integer.parseInt(bookNoteData.get("publishYear")));

        return book;
    }

    private Personage fillPersonageObjectWith(Map<String, String> bookNoteData, Book book) {
        Personage personage = new Personage();

        personage.setFullName(bookNoteData.get("personageFullName"));
        personage.setRole(bookNoteData.get("personageRole"));
        personage.setBook(book);

        return personage;
    }

    private Quote fillQuoteObjectWith(Map<String, String> bookNoteData, Book book, Personage personage) {
        Quote quote = new Quote();

        quote.setBook(book);
        quote.setPersonage(personage);
        quote.setQuotation(bookNoteData.get("quotation"));

        return quote;
    }
}
