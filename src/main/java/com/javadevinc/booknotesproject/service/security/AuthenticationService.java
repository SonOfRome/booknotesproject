package com.javadevinc.booknotesproject.service.security;

import com.javadevinc.booknotesproject.model.User;

import java.util.Optional;

public interface AuthenticationService {
    Optional<User> getUserWith(String login, String password);
}
