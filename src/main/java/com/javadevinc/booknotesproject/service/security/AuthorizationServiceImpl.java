package com.javadevinc.booknotesproject.service.security;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.Role;
import com.javadevinc.booknotesproject.model.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    private DaoFactory daoFactory;

    public AuthorizationServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public Optional<Role> getUserRole(User user) {
        return daoFactory.getRoleDao().getRoleFor(user);
    }

    @Override
    public String getPersonalCabinetPage(String roleName) {
        if (roleName.equals("ADMIN")) {
            return "user/adminCab";
        }
        return "user/userCab";
    }
}
