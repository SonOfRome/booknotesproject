package com.javadevinc.booknotesproject.service.security;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private DaoFactory daoFactory;

    public AuthenticationServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Optional<User> getUserWith(String login, String password) {
        return daoFactory.getUserDao().getUserByHisCredentials(login, password);
    }
}
