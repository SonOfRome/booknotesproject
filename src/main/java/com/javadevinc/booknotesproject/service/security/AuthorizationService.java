package com.javadevinc.booknotesproject.service.security;

public interface AuthorizationService {
    String getPersonalCabinetPage(String roleName);
}
