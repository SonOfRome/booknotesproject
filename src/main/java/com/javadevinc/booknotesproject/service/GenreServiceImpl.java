package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreServiceImpl implements GenreService {
    private DaoFactory daoFactory;

    @Autowired
    public GenreServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void addGenre(Genre genre) {
        daoFactory.getGenreDao().addEntity(genre);
    }
}
