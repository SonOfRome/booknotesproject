package com.javadevinc.booknotesproject.service;

import java.util.Map;

public interface BookNoteService {
    void addBookNote(Map<String, String> bookNoteData);
}
