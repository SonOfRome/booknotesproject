package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.model.Quote;

public interface QuoteService {
    void addQuote(Quote quote);
}
