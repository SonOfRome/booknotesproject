package com.javadevinc.booknotesproject.service;

import com.javadevinc.booknotesproject.dao.DaoFactory;
import com.javadevinc.booknotesproject.model.Personage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonageServiceImpl implements PersonageService {
    private DaoFactory daoFactory;

    @Autowired
    public PersonageServiceImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void addPersonage(Personage personage) {
        daoFactory.getPersonageDao().addEntity(personage);
    }
}
