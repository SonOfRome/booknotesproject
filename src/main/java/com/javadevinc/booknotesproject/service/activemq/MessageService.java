package com.javadevinc.booknotesproject.service.activemq;

public interface MessageService {
    void sendBookNoteNotification(String message);
}
