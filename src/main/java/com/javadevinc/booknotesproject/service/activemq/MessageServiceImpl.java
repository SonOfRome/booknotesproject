package com.javadevinc.booknotesproject.service.activemq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
    private JmsTemplate jmsTemplate;

    @Autowired
    public MessageServiceImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendBookNoteNotification(String message) {
        jmsTemplate.send(session -> session.createTextMessage(message));
    }
}
